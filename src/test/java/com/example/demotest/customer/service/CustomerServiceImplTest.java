package com.example.demotest.customer.service;


import com.example.demotest.customer.entity.Customer;
import com.example.demotest.customer.repository.CustomerRepository;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = CustomerServiceImplTest.class)
class CustomerServiceImplTest {

  @Mock
  private CustomerRepository customerRepository;

  @InjectMocks
  private CustomerServiceImpl customerService;

  List<Customer> fictionList = new ArrayList<>();

  @BeforeEach
  public void init() {
    fictionList.add(Customer.builder().id((long) 1).fullName("Diego Chavez").age(23).build());
  }

  @Test
  @DisplayName("Test01")
  void getAllCustomers_ValidateTest_ListOK() {
    Mockito.when(customerRepository.validate("test")).thenReturn(true);
    Mockito.when(customerRepository.findAll()).thenReturn(fictionList);
    List<Customer> listExpected = customerService.getAllCustomers();
    Assertions.assertThat(listExpected).isNotNull();
    Assertions.assertThat(listExpected.size()).isEqualTo(1);
    Assertions.assertThat(listExpected.get(0).getId().getClass()).isEqualTo(Long.class);
    Mockito.verify(customerRepository, Mockito.times(1)).validate("test");
  }

  @Test
  @DisplayName("Test02")
  void getAllCustomers_ValidateTest_ListNull() {
    Mockito.when(customerRepository.validate("testFake")).thenReturn(false);
    Mockito.when(customerRepository.findAll()).thenReturn(null);
    List<Customer> listExpected = customerService.getAllCustomers();
    Assertions.assertThat(listExpected).isNull();
    Mockito.verify(customerRepository, Mockito.times(1)).validate("test");
  }

}