package com.example.demotest.customer.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Customer {

  private Long id;
  private String fullName;
  private Integer age;

}
