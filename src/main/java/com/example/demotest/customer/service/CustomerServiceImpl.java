package com.example.demotest.customer.service;

import com.example.demotest.customer.entity.Customer;
import com.example.demotest.customer.repository.CustomerRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository customerRepository;

  @Override
  public List<Customer> getAllCustomers() {
    return customerRepository.validate("test") ? customerRepository.findAll() : null;
  }

}
