package com.example.demotest.customer.service;

import com.example.demotest.customer.entity.Customer;
import java.util.List;

public interface CustomerService {

  List<Customer> getAllCustomers();

}
