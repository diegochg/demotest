package com.example.demotest.customer.repository;

import com.example.demotest.customer.entity.Customer;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRepository {

  public List<Customer> findAll() {
    List<Customer> customers = new ArrayList<>();
    customers.add(Customer.builder().id((long) 1).fullName("Diego Chavez").age(23).build());
    customers.add(Customer.builder().id((long) 2).fullName("Jhon Mijahuanja").age(24).build());
    customers.add(Customer.builder().id((long) 3).fullName("Kevin Farias").age(24).build());
    return customers;
  }

  public boolean validate(String parameter) {
    return parameter.equals("test") ? true : false;
  }

}
