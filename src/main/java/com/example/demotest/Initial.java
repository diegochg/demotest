package com.example.demotest;

import com.example.demotest.customer.service.CustomerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class Initial implements ApplicationRunner {

  @Autowired
  private CustomerServiceImpl customerService;

  @Override
  public void run(ApplicationArguments args) throws Exception {
    customerService.getAllCustomers().forEach(System.out::println);
  }

}
